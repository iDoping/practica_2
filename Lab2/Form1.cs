﻿using System.Windows.Forms;
using Lab2Lib;

namespace Lab2
{
    public partial class Form1 : Form
    {
        //Я реализовал использование библиотеки через ссылку на проект
        //в котором реализован нужный нам класс
        public Form1()
        {
            InitializeComponent();
        }

        private void simple_Rate_Click(object sender, System.EventArgs e)
        {
            firstResult.Text = Rate.SimpleRate(initialSum.Text, percentRate.Text, periodCount.Text);
        }

        private void hard_Rate_Click(object sender, System.EventArgs e)
        {
            secondResult.Text = Rate.HardRate(initialSumh.Text, periodCounth.Text, periodCounth.Text);
        }

        private void nominal_Rate_Click(object sender, System.EventArgs e)
        {
            thirdResult.Text = Rate.NominalRate(initialSumn.Text, yearRate.Text, percents.Text, periodCountn.Text);
        }

        private void grow_Power_Click(object sender, System.EventArgs e)
        {
            firstResult.Text = Rate.GrowPower(initialSumg.Text, eVal.Text, periodCountg.Text, deltaVal.Text);
        }

        private void real_Rate_Click(object sender, System.EventArgs e)
        {
            fifthResult.Text = Rate.RealRate(nominalRate.Text, piVal.Text);
        }
    }
}
