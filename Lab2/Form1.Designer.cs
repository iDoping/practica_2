﻿
namespace Lab2
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.firstResult = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.simple_Rate = new System.Windows.Forms.Button();
            this.periodCount = new System.Windows.Forms.TextBox();
            this.percentRate = new System.Windows.Forms.TextBox();
            this.initialSum = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.hard_Rate = new System.Windows.Forms.Button();
            this.secondResult = new System.Windows.Forms.TextBox();
            this.periodCounth = new System.Windows.Forms.TextBox();
            this.percentRateh = new System.Windows.Forms.TextBox();
            this.initialSumh = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.nominal_Rate = new System.Windows.Forms.Button();
            this.periodCountn = new System.Windows.Forms.TextBox();
            this.thirdResult = new System.Windows.Forms.TextBox();
            this.initialSumn = new System.Windows.Forms.TextBox();
            this.percents = new System.Windows.Forms.TextBox();
            this.yearRate = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.grow_Power = new System.Windows.Forms.Button();
            this.fourthResult = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.initialSumg = new System.Windows.Forms.TextBox();
            this.periodCountg = new System.Windows.Forms.TextBox();
            this.deltaVal = new System.Windows.Forms.TextBox();
            this.eVal = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.nominalRate = new System.Windows.Forms.TextBox();
            this.piVal = new System.Windows.Forms.TextBox();
            this.fifthResult = new System.Windows.Forms.TextBox();
            this.real_Rate = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(933, 297);
            this.tabControl1.TabIndex = 40;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.firstResult);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.simple_Rate);
            this.tabPage1.Controls.Add(this.periodCount);
            this.tabPage1.Controls.Add(this.percentRate);
            this.tabPage1.Controls.Add(this.initialSum);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(925, 269);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Простая процентная ставка";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.hard_Rate);
            this.tabPage2.Controls.Add(this.secondResult);
            this.tabPage2.Controls.Add(this.periodCounth);
            this.tabPage2.Controls.Add(this.percentRateh);
            this.tabPage2.Controls.Add(this.initialSumh);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(925, 269);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Сложная процентная ставка";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.nominal_Rate);
            this.tabPage3.Controls.Add(this.periodCountn);
            this.tabPage3.Controls.Add(this.thirdResult);
            this.tabPage3.Controls.Add(this.initialSumn);
            this.tabPage3.Controls.Add(this.percents);
            this.tabPage3.Controls.Add(this.yearRate);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(925, 269);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Номинальная процентная ставка";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.grow_Power);
            this.tabPage4.Controls.Add(this.fourthResult);
            this.tabPage4.Controls.Add(this.label18);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.initialSumg);
            this.tabPage4.Controls.Add(this.periodCountg);
            this.tabPage4.Controls.Add(this.deltaVal);
            this.tabPage4.Controls.Add(this.eVal);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(925, 269);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Непрерывно начисляемые проценты";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.real_Rate);
            this.tabPage5.Controls.Add(this.fifthResult);
            this.tabPage5.Controls.Add(this.piVal);
            this.tabPage5.Controls.Add(this.nominalRate);
            this.tabPage5.Controls.Add(this.label22);
            this.tabPage5.Controls.Add(this.label21);
            this.tabPage5.Controls.Add(this.label20);
            this.tabPage5.Location = new System.Drawing.Point(4, 24);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(925, 269);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Реальная процентная ставка";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // firstResult
            // 
            this.firstResult.Location = new System.Drawing.Point(402, 162);
            this.firstResult.Name = "firstResult";
            this.firstResult.ReadOnly = true;
            this.firstResult.Size = new System.Drawing.Size(140, 23);
            this.firstResult.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(310, 165);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 15);
            this.label4.TabIndex = 16;
            this.label4.Text = "Результат";
            // 
            // simple_Rate
            // 
            this.simple_Rate.Location = new System.Drawing.Point(427, 191);
            this.simple_Rate.Name = "simple_Rate";
            this.simple_Rate.Size = new System.Drawing.Size(84, 23);
            this.simple_Rate.TabIndex = 15;
            this.simple_Rate.Text = "Рассчитать";
            this.simple_Rate.UseVisualStyleBackColor = true;
            // 
            // periodCount
            // 
            this.periodCount.Location = new System.Drawing.Point(402, 127);
            this.periodCount.Name = "periodCount";
            this.periodCount.Size = new System.Drawing.Size(140, 23);
            this.periodCount.TabIndex = 14;
            // 
            // percentRate
            // 
            this.percentRate.Location = new System.Drawing.Point(402, 92);
            this.percentRate.Name = "percentRate";
            this.percentRate.Size = new System.Drawing.Size(140, 23);
            this.percentRate.TabIndex = 13;
            // 
            // initialSum
            // 
            this.initialSum.Location = new System.Drawing.Point(402, 56);
            this.initialSum.Name = "initialSum";
            this.initialSum.Size = new System.Drawing.Size(140, 23);
            this.initialSum.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(203, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(167, 15);
            this.label3.TabIndex = 11;
            this.label3.Text = "Число периодов начисления";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(139, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(231, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "Процентная ставка, выраженная в долях";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(271, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 15);
            this.label1.TabIndex = 9;
            this.label1.Text = "Исходная сумма";
            // 
            // hard_Rate
            // 
            this.hard_Rate.Location = new System.Drawing.Point(424, 199);
            this.hard_Rate.Name = "hard_Rate";
            this.hard_Rate.Size = new System.Drawing.Size(83, 23);
            this.hard_Rate.TabIndex = 26;
            this.hard_Rate.Text = "Рассчитать";
            this.hard_Rate.UseVisualStyleBackColor = true;
            // 
            // secondResult
            // 
            this.secondResult.Location = new System.Drawing.Point(396, 170);
            this.secondResult.Name = "secondResult";
            this.secondResult.ReadOnly = true;
            this.secondResult.Size = new System.Drawing.Size(140, 23);
            this.secondResult.TabIndex = 25;
            // 
            // periodCounth
            // 
            this.periodCounth.Location = new System.Drawing.Point(396, 135);
            this.periodCounth.Name = "periodCounth";
            this.periodCounth.Size = new System.Drawing.Size(140, 23);
            this.periodCounth.TabIndex = 24;
            // 
            // percentRateh
            // 
            this.percentRateh.Location = new System.Drawing.Point(396, 100);
            this.percentRateh.Name = "percentRateh";
            this.percentRateh.Size = new System.Drawing.Size(140, 23);
            this.percentRateh.TabIndex = 23;
            // 
            // initialSumh
            // 
            this.initialSumh.Location = new System.Drawing.Point(396, 64);
            this.initialSumh.Name = "initialSumh";
            this.initialSumh.Size = new System.Drawing.Size(140, 23);
            this.initialSumh.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(303, 173);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 15);
            this.label8.TabIndex = 21;
            this.label8.Text = "Результат";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(196, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(167, 15);
            this.label7.TabIndex = 20;
            this.label7.Text = "Число периодов начисления";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(132, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(231, 15);
            this.label6.TabIndex = 19;
            this.label6.Text = "Процентная ставка, выраженная в долях";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(264, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 15);
            this.label5.TabIndex = 18;
            this.label5.Text = "Исходная сумма";
            // 
            // nominal_Rate
            // 
            this.nominal_Rate.Location = new System.Drawing.Point(429, 199);
            this.nominal_Rate.Name = "nominal_Rate";
            this.nominal_Rate.Size = new System.Drawing.Size(84, 23);
            this.nominal_Rate.TabIndex = 39;
            this.nominal_Rate.Text = "Рассчитать";
            this.nominal_Rate.UseVisualStyleBackColor = true;
            // 
            // periodCountn
            // 
            this.periodCountn.Location = new System.Drawing.Point(404, 141);
            this.periodCountn.Name = "periodCountn";
            this.periodCountn.Size = new System.Drawing.Size(140, 23);
            this.periodCountn.TabIndex = 38;
            // 
            // thirdResult
            // 
            this.thirdResult.Location = new System.Drawing.Point(404, 173);
            this.thirdResult.Name = "thirdResult";
            this.thirdResult.ReadOnly = true;
            this.thirdResult.Size = new System.Drawing.Size(140, 23);
            this.thirdResult.TabIndex = 37;
            // 
            // initialSumn
            // 
            this.initialSumn.Location = new System.Drawing.Point(404, 114);
            this.initialSumn.Name = "initialSumn";
            this.initialSumn.Size = new System.Drawing.Size(140, 23);
            this.initialSumn.TabIndex = 36;
            // 
            // percents
            // 
            this.percents.Location = new System.Drawing.Point(404, 84);
            this.percents.Name = "percents";
            this.percents.Size = new System.Drawing.Size(140, 23);
            this.percents.TabIndex = 35;
            // 
            // yearRate
            // 
            this.yearRate.Location = new System.Drawing.Point(404, 53);
            this.yearRate.Name = "yearRate";
            this.yearRate.Size = new System.Drawing.Size(140, 23);
            this.yearRate.TabIndex = 34;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(312, 173);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 15);
            this.label13.TabIndex = 33;
            this.label13.Text = "Результат";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(205, 144);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(167, 15);
            this.label12.TabIndex = 32;
            this.label12.Text = "Число периодов начисления";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(273, 114);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 15);
            this.label11.TabIndex = 31;
            this.label11.Text = "Исходная сумма";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(308, 84);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 15);
            this.label10.TabIndex = 30;
            this.label10.Text = "Проценты";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(215, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(157, 15);
            this.label9.TabIndex = 29;
            this.label9.Text = "Годовая процентная ставка";
            // 
            // grow_Power
            // 
            this.grow_Power.Location = new System.Drawing.Point(420, 191);
            this.grow_Power.Name = "grow_Power";
            this.grow_Power.Size = new System.Drawing.Size(83, 23);
            this.grow_Power.TabIndex = 50;
            this.grow_Power.Text = "Рассчитать";
            this.grow_Power.UseVisualStyleBackColor = true;
            // 
            // fourthResult
            // 
            this.fourthResult.Location = new System.Drawing.Point(392, 162);
            this.fourthResult.Name = "fourthResult";
            this.fourthResult.ReadOnly = true;
            this.fourthResult.Size = new System.Drawing.Size(140, 23);
            this.fourthResult.TabIndex = 49;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(299, 168);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(60, 15);
            this.label18.TabIndex = 48;
            this.label18.Text = "Результат";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(260, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(99, 15);
            this.label17.TabIndex = 47;
            this.label17.Text = "Исходная сумма";
            // 
            // initialSumg
            // 
            this.initialSumg.Location = new System.Drawing.Point(392, 48);
            this.initialSumg.Name = "initialSumg";
            this.initialSumg.Size = new System.Drawing.Size(140, 23);
            this.initialSumg.TabIndex = 46;
            // 
            // periodCountg
            // 
            this.periodCountg.Location = new System.Drawing.Point(392, 135);
            this.periodCountg.Name = "periodCountg";
            this.periodCountg.Size = new System.Drawing.Size(140, 23);
            this.periodCountg.TabIndex = 45;
            // 
            // deltaVal
            // 
            this.deltaVal.Location = new System.Drawing.Point(392, 105);
            this.deltaVal.Name = "deltaVal";
            this.deltaVal.Size = new System.Drawing.Size(140, 23);
            this.deltaVal.TabIndex = 44;
            // 
            // eVal
            // 
            this.eVal.Location = new System.Drawing.Point(392, 77);
            this.eVal.Name = "eVal";
            this.eVal.Size = new System.Drawing.Size(140, 23);
            this.eVal.TabIndex = 43;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(192, 141);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(167, 15);
            this.label16.TabIndex = 42;
            this.label16.Text = "Число периодов начисления";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(324, 111);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 15);
            this.label15.TabIndex = 41;
            this.label15.Text = "Delta";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(345, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 15);
            this.label14.TabIndex = 40;
            this.label14.Text = "e";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(224, 94);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(190, 15);
            this.label20.TabIndex = 1;
            this.label20.Text = "Номинальная процентная ставка";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(126, 123);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(288, 15);
            this.label21.TabIndex = 2;
            this.label21.Text = "Ожидаемый или планируемый уровень инфляции";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(354, 154);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 15);
            this.label22.TabIndex = 3;
            this.label22.Text = "Результат";
            // 
            // nominalRate
            // 
            this.nominalRate.Location = new System.Drawing.Point(421, 91);
            this.nominalRate.Name = "nominalRate";
            this.nominalRate.Size = new System.Drawing.Size(140, 23);
            this.nominalRate.TabIndex = 5;
            // 
            // piVal
            // 
            this.piVal.Location = new System.Drawing.Point(421, 123);
            this.piVal.Name = "piVal";
            this.piVal.Size = new System.Drawing.Size(140, 23);
            this.piVal.TabIndex = 6;
            // 
            // fifthResult
            // 
            this.fifthResult.Location = new System.Drawing.Point(421, 154);
            this.fifthResult.Name = "fifthResult";
            this.fifthResult.ReadOnly = true;
            this.fifthResult.Size = new System.Drawing.Size(140, 23);
            this.fifthResult.TabIndex = 7;
            // 
            // real_Rate
            // 
            this.real_Rate.Location = new System.Drawing.Point(454, 183);
            this.real_Rate.Name = "real_Rate";
            this.real_Rate.Size = new System.Drawing.Size(80, 23);
            this.real_Rate.TabIndex = 8;
            this.real_Rate.Text = "Рассчитать";
            this.real_Rate.UseVisualStyleBackColor = true;
            this.real_Rate.Click += new System.EventHandler(this.real_Rate_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 325);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Lab2";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox firstResult;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button simple_Rate;
        private System.Windows.Forms.TextBox periodCount;
        private System.Windows.Forms.TextBox percentRate;
        private System.Windows.Forms.TextBox initialSum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button hard_Rate;
        private System.Windows.Forms.TextBox secondResult;
        private System.Windows.Forms.TextBox periodCounth;
        private System.Windows.Forms.TextBox percentRateh;
        private System.Windows.Forms.TextBox initialSumh;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button nominal_Rate;
        private System.Windows.Forms.TextBox periodCountn;
        private System.Windows.Forms.TextBox thirdResult;
        private System.Windows.Forms.TextBox initialSumn;
        private System.Windows.Forms.TextBox percents;
        private System.Windows.Forms.TextBox yearRate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button grow_Power;
        private System.Windows.Forms.TextBox fourthResult;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox initialSumg;
        private System.Windows.Forms.TextBox periodCountg;
        private System.Windows.Forms.TextBox deltaVal;
        private System.Windows.Forms.TextBox eVal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button real_Rate;
        private System.Windows.Forms.TextBox fifthResult;
        private System.Windows.Forms.TextBox piVal;
        private System.Windows.Forms.TextBox nominalRate;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
    }
}

