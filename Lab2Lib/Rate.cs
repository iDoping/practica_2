﻿using System;

namespace Lab2Lib
{
    public class Rate
    {
        public static string SimpleRate(string p, string i, string n)
        {
            return (Double.Parse(p) + Double.Parse(p) * Double.Parse(n) * Double.Parse(i)).ToString();
        }

        public static string HardRate(string p, string i, string n)
        {
            return (Math.Pow(1 + Double.Parse(i), Double.Parse(n)) * Double.Parse(p)).ToString();
        }

        public static string NominalRate(string p, string j, string n, string m)
        {
            return (Math.Pow(1 + (Double.Parse(j) / Double.Parse(m)), Double.Parse(n)) * Double.Parse(p)).ToString();
        }

        public static string GrowPower(string p, string e, string n, string delta)
        {
            return (Math.Pow(Double.Parse(e), Double.Parse(delta) * Double.Parse(n)) * Double.Parse(p)).ToString();
        }

        public static string RealRate(string inRate, string pi)
        {
            return ((1 + Double.Parse(inRate)) / 1 + (Double.Parse(pi)) - 1).ToString();
        }
    }
    //Возник вопрос, как реализовывать непрерывно начисляемые проценты:
    //необходимо передавать значения в качестве параметров, 
    //либо вычислять некоторые параметры уже реализованными функциями
}
